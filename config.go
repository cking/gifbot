package main

import (
	"github.com/caarlos0/env"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog/log"
)

type config struct {
	MastodonURL    string `env:"MASTO_URL,required"`
	MastodonKey    string `env:"MASTO_KEY,required"`
	MastodonSecret string `env:"MASTO_SECRET,required"`

	Mail string `env:"MASTO_MAIL,required"`
	Pass string `env:"MASTO_PASS,required"`

	Mongo  string `env:"MONGO,required"`
	Assets string `env:"ASSETS" envDefault:"./assets"`

	Cron int `env:"CRON" envDefault:"23"`
}

func loadConfiguration() *config {
	log.Debug().Msg("Loading <gifbot.conf>")
	// load gifbot
	err := godotenv.Load("gifbot.conf")
	if err != nil {
		log.Error().Err(err).Msg("Failed to read configuration")
	}

	cfg := config{}
	err = env.Parse(&cfg)
	if err != nil {
		log.Error().Err(err).Msg("Failed to parse configuration")
	}

	return &cfg
}
