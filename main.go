package main

import (
	"math/rand"
	"net/url"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"strings"
	"syscall"
	"time"

	"github.com/McKael/madon"
	"github.com/globalsign/mgo"
	"github.com/kennygrant/sanitize"
	// "github.com/kennygrant/sanitize"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/pflag"
)

var (
	verboseFlag = pflag.BoolP("verbose", "v", false, "more verbose output")
	debugFlag   = pflag.BoolP("debug", "d", false, "debug mode (dont cron, end after one cycle)")
	dryrunFlag  = pflag.BoolP("dryrun", "n", false, "dont post status updates")
)

func init() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout, NoColor: runtime.GOOS == "windows"})
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
}

func connectToMongo(cfg *config) (*mgo.Database, error) {
	log.Debug().Str("url", cfg.Mongo).Msg("Connecting to database...")
	mongo, err := mgo.Dial(cfg.Mongo)
	if err != nil {
		return nil, err
	}

	dbName := ApplicationName
	parsedURL, err := url.Parse(cfg.Mongo)
	if err == nil && parsedURL.Path != "" {
		dbName = parsedURL.Path[1:]
	}

	return mongo.DB(dbName), nil
}

type result struct {
	File   string "file"
	Source string "source"
	Mood   string "mood"
	NSFW   bool   "nsfw"
}

func tagify(input string) string {
	output := strings.ToLower(input)
	output = sanitize.Name(output)
	output = strings.Replace(output, "-", "_", -1)
	return output
}

func main() {
	pflag.Parse()
	if *verboseFlag {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	log.Info().Msgf("Booting %v %v", ApplicationName, ApplicationVersion)
	cfg := loadConfiguration()

	log.Debug().Str("instance", cfg.MastodonURL).Msg("Connecting to instance...")
	client, err := madon.RestoreApp(ApplicationName, cfg.MastodonURL, cfg.MastodonKey, cfg.MastodonSecret, nil)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to restore client instance")
	}

	if err := client.LoginBasic(cfg.Mail, cfg.Pass, []string{"read", "write", "follow"}); err != nil {
		log.Fatal().Err(err).Msg("Failed to login")
	}

	db, err := connectToMongo(cfg)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to connect to db")
	}
	collection := db.C("images")

	rand.Seed(int64(time.Now().Nanosecond()))

	action := func() {
		log.Debug().Msg("Preparing to fetch new entry...")
		n, err := collection.Count()
		if err != nil {
			log.Error().Err(err).Msg("Failed to fetch count")
			return
		}
		docNum := rand.Intn(n)

		log.Debug().Int("idx", docNum).Int("max", n).Msg("Fetching document...")
		var response result
		if err := collection.Find(nil).Limit(-1).Skip(docNum).One(&response); err != nil {
			log.Error().Err(err).Msg("Failed to fetch document")
			return
		}

		log.Debug().Msg("Preparing message body...")
		var body strings.Builder
		body.WriteString("Source: #")
		body.WriteString(tagify(response.Source))
		body.WriteRune('\n')
		body.WriteString(response.Mood)

		if !*dryrunFlag {
			log.Debug().Msg("Uploading image...")
			attachment, err := client.UploadMedia(filepath.Join(cfg.Assets, response.File), response.Source, "")
			if err != nil {
				log.Error().Err(err).Msg("Failed to upload image")
				return
			}

			log.Debug().Msg("Posting status...")
			media := []int64{attachment.ID}
			_, err = client.PostStatus(body.String(), -1, media, response.NSFW, "", "public")
			if err != nil {
				log.Error().Err(err).Msg("Failed to post status")
				return
			}
		}

		log.Info().Msg("Posted a new message!")
	}

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)

	if *debugFlag {
		action()
	} else {
		go func() {
			for {
				t := time.Now()
				if t.Minute() == cfg.Cron && t.Second() <= 15 {
					action()
				}
				time.Sleep(15 * time.Second)
			}
		}()
	}

	<-sc
}
